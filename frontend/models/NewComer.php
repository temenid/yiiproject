<?php

namespace frontend\models;

use Yii;
use yii\base\Model;

use yii\web\Cookie;
use yii\web\Request;
use yii\web\Response;

class NewComer extends Model{
	public function getWas_here_attr(){
		$expire=time()+60;
		
		if (!isset(Yii::$app->request->cookies['was_here'])) {
			$cookies = Yii::$app->response->cookies;	
			$cookies->add(new \yii\web\Cookie([
				'name' => 'was_here',
				'value' => true,
				'expire' => $expire
			]));
			return false;
		}else{
			$cookies = Yii::$app->request->cookies;
			$cookies['was_here']->expire=$expire;
			return true;
		}
	
	}
}
