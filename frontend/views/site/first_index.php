<?php
/* @var $this yii\web\View */
$this->title = 'You are welcome';

use yii\web\View;

\frontend\assets\AppAsset::register($this);  // $this represents the view object

$this->registerCssFile("/css/first_index.css");
?>
<div class="site-index">
	<div class="welcome">
		<h1>Welcome to <span class="we_are" style="font-family:sans-serif"> demo site </strong></h1>		
	</div> 
	
	<div class="slogan">
		<h4>великий код начинается с маленьких шагов.</h3>
	</div>
	  
	<div class="join_us">
		<a href="/" id="main_button">join <b>+</b></a>
	</div>

	<div class="action">
		<h4>join us</h3>
	</div>
    
	<div class="we_are">
		<p class="school">junior developer</p>		
	</div> 
</div>
